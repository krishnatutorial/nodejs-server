module.exports = {
  'GET /private/users': 'UserController.getAll',
  'POST /private/item': 'ItemController.add',
  'GET /private/items': 'ItemController.getAll',
  'DELETE /private/item/:id': 'ItemController.destroy',
  'PUT /private/item/:id': 'ItemController.update',
};
