module.exports = {
  'POST /public/user': 'UserController.register',
  'POST /public/register': 'UserController.register', // alias for POST /user
  'POST /public/login': 'UserController.login',
  'POST /public/validate': 'UserController.validate',
};
