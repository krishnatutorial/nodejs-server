const Item = require('../models/Item');
const authService = require('../services/auth.service');
const bcryptService = require('../services/bcrypt.service');

const ItemController = () => {
  const add = (req, res) => {
      
    const body = req.body;
    return Item
      .create({
        name: body.name,
        price: body.price,
      })
      .then((Item) => {
        return res.status(200).json({ Item });
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });

    return res.status(400).json({ msg: 'Passwords don\'t match' });
  };

  const update = (req, res) => {
    const id = req.body.id;
    Item
      .findOne({
        where: {
          id,
        },
      })
      .then((Item) => {

          if (!Item) {
            return res.status(400).json({ msg: 'Bad Request: Item not found' });
          }
        
          Item.update({
            name: req.body.name || Item.name,
            price: req.body.price || Item.price,
          }).then((Item) => {
            return res.status(200).json({ Item });
          })
          .catch((err) => {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
          });
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  const destroy = (req, res) => {
    const id = req.params.id;
    console.log("param:",id)
    Item
      .findOne({
        where: {
          id,
        },
      })
      .then((Item) => {
        console.log(Item)
        if (!Item) {
          return res.status(400).json({ msg: 'Bad Request: Item not found' });
        }

        Item.destroy().then(() => {
          return res.status(200).json({ msg: 'Item delete successfull' });
        })
        .catch((err) => {
          console.log(err);
          return res.status(500).json({ msg: 'Internal server error' });
        });
        
      })
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };

  const getAll = (req, res) => {
    Item
      .findAll()
      .then((Item) => res.status(200).json({ Item }))
      .catch((err) => {
        console.log(err);
        return res.status(500).json({ msg: 'Internal server error' });
      });
  };


  return {
    add,
    update,
    destroy,
    getAll,
  };
};

module.exports = ItemController;
