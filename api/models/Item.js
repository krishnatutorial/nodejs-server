const Sequelize = require('sequelize');
const bcryptSevice = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const tableName = 'Items';

const Item = sequelize.define('Item', {
  name: {
    type: Sequelize.STRING,
  },
  price: {
    type: Sequelize.STRING,
  },
}, {  tableName });

module.exports = Item;
